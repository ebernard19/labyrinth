﻿using UnityEngine;
using System.Collections;

public class DoNotFall : MonoBehaviour
{
	private bool alreadyDeclared;
	
	// Use this for initialization
	void Start()
	{
		alreadyDeclared=false;
	}
	
	// Update is called once per frame
	void Update()
	{
		if(!alreadyDeclared)
		{
			if(transform.position.y<-100)
			{
				SendMessage("RespawnBall");
				alreadyDeclared=true;
			}
		}
		else
		{
			if(transform.position.y>-100)
			{
				alreadyDeclared=false;
			}
		}
	}
}
