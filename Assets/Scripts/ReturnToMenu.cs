﻿using UnityEngine;
using System.Collections;

public class ReturnToMenu : MonoBehaviour
{
	public string menuName="Menu";

	// Use this for initialization
	void Start()
	{
	
	}
	
	// Update is called once per frame
	void Update()
	{
		InputPC();
	}
	
	void InputPC()
	{
		if(Input.GetKey(KeyCode.Escape))
		{
			Application.LoadLevel(menuName);
		}
	}
}
