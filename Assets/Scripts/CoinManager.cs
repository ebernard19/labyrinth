﻿using UnityEngine;
using System.Collections;

public class CoinManager : MonoBehaviour
{
	static private int coinsLeft=0;
	
	// Use this for initialization
	void Start ()
	{

	}

	void Awake()
	{
		coinsLeft = 0;
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	static public void AddCoin()
	{
		coinsLeft++;
	}

	static public void RemoveOneCoin()
	{
		coinsLeft--;
		Debug.Log ("Coin picked ! Left : "+GetCoins ());
	}

	static public int GetCoins()
	{
		return coinsLeft;
	}
}
