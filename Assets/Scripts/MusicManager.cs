﻿using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour
{
	public AudioClip Level;
	public AudioClip Ending;
	public AudioClip Lose;
	
	// Use this for initialization
	void Start()
	{
		LevelMusic();
	}
	
	// Update is called once per frame
	void Update()
	{
		
	}
	
	void LevelMusic()
	{
		audio.Stop();
		audio.clip=Level;
		audio.loop=true;
		audio.Play();
	}
	
	void LoseMusic()
	{
		audio.Stop();
		audio.clip=Lose;
		audio.loop=false;
		audio.Play();
	}
	
	void FinishMusic()
	{
		audio.Stop();
		audio.clip=Ending;
		audio.loop=false;
		audio.Play();
	}
}
