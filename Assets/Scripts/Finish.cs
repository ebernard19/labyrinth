﻿using UnityEngine;
using System.Collections;

public class Finish : MonoBehaviour
{
	public GUIText GUIWin;
	public int trophyId;
	public uint id;
	
	// Use this for initialization
	void Start()
	{
		GUIWin.enabled=false;
		id=(uint)trophyId;
	}
	
	// Update is called once per frame
	void Update()
	{
	
	}
	
	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player")
        {
            GUIWin.enabled=true;
			GJAPI.Trophies.Add(id);
			GJAPIHelper.Trophies.ShowTrophyUnlockNotification(id);
			Camera.main.SendMessage("FinishMusic");
			Camera.main.animation.Play("Finished");
        }
	}
}
