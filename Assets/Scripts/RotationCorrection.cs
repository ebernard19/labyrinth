﻿using UnityEngine;
using System.Collections;

public class RotationCorrection : MonoBehaviour
{
	// Use this for initialization
	void Start()
	{
	
	}
	
	// Update is called once per frame
	void Update()
	{
		Vector3 tmp = transform.eulerAngles;
		
		if(tmp.y!=0)
		{
			tmp.y = 0;
		}
		
		transform.eulerAngles = tmp;
	}
}
