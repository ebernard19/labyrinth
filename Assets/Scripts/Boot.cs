﻿using UnityEngine;
using System.Collections;

public class Boot : MonoBehaviour {

	// Use this for initialization
	void Start () {
		this.SendMessage ("booting");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator booting()
	{
		yield return new WaitForSeconds(2);
		Application.LoadLevel (1);
	}
}
