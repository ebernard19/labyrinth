﻿using UnityEngine;
using System.Collections;

public class CameraOnBall : MonoBehaviour
{
    public GameObject theBall;

	// Use this for initialization
	void Start()
    {
	    
	}
	
	// Update is called once per frame
	void Update()
    {
        Vector3 tmp = transform.position;
        tmp.x = theBall.transform.position.x;
        tmp.y = theBall.transform.position.y + 10;
        tmp.z = theBall.transform.position.z - 10;
        transform.position = tmp;
	}
	
	void endOfAnim()
	{
		Application.LoadLevel("Menu");
	}
}
