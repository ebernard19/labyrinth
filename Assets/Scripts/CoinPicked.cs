﻿using UnityEngine;
using System.Collections;

public class CoinPicked : MonoBehaviour {

	// Use this for initialization
	void Start ()
	{
		CoinManager.AddCoin();
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player")
		{
			CoinManager.RemoveOneCoin();
			audio.Play ();
			Destroy(gameObject);
		}
	}
}
