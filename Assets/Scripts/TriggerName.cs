﻿using UnityEngine;
using System.Collections;

public class TriggerName : MonoBehaviour
{
	public string nameOfLevel;
	
	// Use this for initialization
	void Start()
	{
	
	}
	
	// Update is called once per frame
	void Update()
	{
	
	}
	
	void OnTriggerEnter(Collider other)
	{
		if(other.tag=="Player")
		{
			other.SendMessage("printName",nameOfLevel);
		}
	}
}