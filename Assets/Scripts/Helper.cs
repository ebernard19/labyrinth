﻿using UnityEngine;
using System.Collections;

public class Helper : MonoBehaviour
{

	// Use this for initialization
	void Start()
	{
		if(Application.isEditor)
		{
			OnGetFromWeb("ebernard19","lne5002rh");
		}
	}
	
	void OnTriggerEnter(Collider other)
	{
		if(other.tag=="Player")
		{
			if(Application.isEditor)
			{
				OnGetFromWeb("ebernard19","lne5002rh");
			}
			else
			{
				Screen.showCursor=true;
				GJAPIHelper.Users.ShowLogin();
			}
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}
	
	void OnGUI()
	{
		
	}
	
	void OnGetFromWeb (string name, string token)
	{
    	GJAPI.Users.Verify(name,token);
	}
}
