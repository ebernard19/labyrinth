﻿using UnityEngine;
using System.Collections;

public class AutoVerticalMove : MonoBehaviour
{
	public int max;
	private int i=0;
	public int dir=1;
	private int phase=1;
	
	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		this.transform.Translate (0f, 0f, phase*dir*0.05f);
		
		i += phase;
		if (i > max)
		{
			phase=-1;
		}
		else if(i<0)
		{
			phase=1;
		}
	}
}
