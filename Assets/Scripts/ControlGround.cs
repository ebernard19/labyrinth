﻿using UnityEngine;
using System.Collections;

public class ControlGround : MonoBehaviour
{
	private bool control;
	
	// Use this for initialization
	void Start()
    {
		control=true;
		Screen.showCursor=false;
		if(Application.platform==RuntimePlatform.Android)
		{
			Screen.orientation=ScreenOrientation.Landscape;
		}
	}
	
	// Update is called once per frame
	void Update()
    {
        InputPC();
	}

    void InputPC()
    {
		if(Application.platform!=RuntimePlatform.Android)
		{
	        float h=Input.GetAxis("Mouse X");
	        float v=Input.GetAxis("Mouse Y");
			
			Vector3 tmp=transform.eulerAngles;
			
			if(!control)
			{
				return;
			}
			
			if(v>0)
			{
				if(tmp.x<15||tmp.x>180)
				{
					transform.Rotate(v,0,0);
				}
			}
			else
			{
				if(tmp.x>345||tmp.x<180)
				{
					transform.Rotate(v,0,0);
				}
			}
			if(h<0)
			{
				if(tmp.z<15||tmp.z>180)
				{
					transform.Rotate(0,0,-h);
				}
			}
			else
			{
				if(tmp.z>345||tmp.z<180)
				{
					transform.Rotate(v,0,-h);
				}
			}
		}
		else
		{
			float iPx=Input.acceleration.x*20;
			float iPy=Input.acceleration.y*20;
			Vector3 tmp=transform.eulerAngles;
			
			tmp.x=iPy;
			tmp.z=-iPx;
			
			transform.eulerAngles=tmp;
		}
    }
	
	void dontControling()
	{
		control=false;
	}
	
	void controling()
	{
		control=true;
	}
}
