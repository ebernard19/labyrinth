﻿using UnityEngine;
using System.Collections;

public class OpenWithCoins : MonoBehaviour
{
	private bool opened=false;

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (!opened)
		{
			if (CoinManager.GetCoins()==0)
			{
				this.transform.Translate (0f, -3f, 0f);
				opened=true;
			}
		}
	}
}
