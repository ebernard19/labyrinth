﻿using UnityEngine;
using System.Collections;

public class ButtonControl : MonoBehaviour {
	
	public GameObject theWall;
	public Texture texturePushed;
	private bool pushed;
	
	// Use this for initialization
	void Start()
	{
		pushed=false;
	}
	
	// Update is called once per frame
	void Update()
	{
	
	}
	
	void OnTriggerEnter(Collider other)
	{
		if(other.tag=="Player"&&!pushed)
		{
			Vector3 tmp=theWall.transform.localPosition;
			tmp.y+=3;
			theWall.transform.localPosition=tmp;
			Destroy(theWall.collider);
			renderer.material.mainTexture=texturePushed;
			pushed=true;
			audio.Play();
		}
	}
}
