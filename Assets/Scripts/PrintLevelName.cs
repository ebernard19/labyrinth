﻿using UnityEngine;
using System.Collections;

public class PrintLevelName : MonoBehaviour
{
	public GUIText theText;
	
	// Use this for initialization
	void Start()
	{
		theText.enabled=false;
	}
	
	// Update is called once per frame
	void Update()
	{
		
	}
	
	void doNotPrint()
	{
		theText.enabled=false;
	}
	
	void printName(string n)
	{
		theText.text=n;
		theText.enabled=true;
	}
}
