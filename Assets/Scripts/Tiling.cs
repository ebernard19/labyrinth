﻿using UnityEngine;
using System.Collections;

public class Tiling : MonoBehaviour {

	// Use this for initialization
	void Start ()
	{
		Vector2 SS = renderer.material.mainTextureScale;
		SS.x = transform.lossyScale.x;
		SS.y = transform.lossyScale.z;
		renderer.material.mainTextureScale = SS;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
